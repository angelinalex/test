import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Logins from './Login';
import Selection from './selection';
import Password from './password';
import Reset from './reset';






const App = () => {
    return (
        <div>
            <BrowserRouter>
                <div>
                   
                    <Route path="/selection" exact component={Selection} />
                    <Route path="/" exact component={Logins}/>
                  <Route path="/password" exact component={Password}/>
                       <Route path="/reset" exact component={Reset}/>
                   
                </div>
            </BrowserRouter>
        </div>
    );
}

export default App;