import React from 'react';
import './_loginSty.scss';
import './reset';
import { Link } from "react-router-dom";



class Reset extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoginOpen: true
    
    };
  }


  render() {

    return (
      <div>



<nav className="navbar navbar-expand-md bg-dark navbar-dark">
        <a className="navbar-brand" href="/">My Portfolio</a>
      
       
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span className="navbar-toggler-icon"></span>
        </button>
      
   
        <div className="collapse navbar-collapse" id="collapsibleNavbar">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item">
              <a className="nav-link" href="/">Home</a>
            </li>
            <li className="nav-item"> 
              <a className="nav-link" href="/">How It Works</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/">Pricing</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="//">Contact Us</a>
              </li>
              <li className="nav-item">
              <button className="ui button">
              <Link to="/selection"> Login/Register</Link>
                </button>
  
              </li>
          </ul>
        </div>
      </nav>
<br/>

      <div className="root-container">
        
<br/>

    
      <br/>
      <div className="box-container">
      {this.state.isLoginOpen && <Forget/>}
    
      </div>
      </div>
      </div>
    );
  }
}
class Forget extends React.Component {

  constructor(props) {
    super(props);
    this.state = {username:"",password:"",errors:[],pwdState:null};
    
  }
  showValidationErr(elm, msg) {
    this.setState((prevState) => ({
      errors: [
        ...prevState.errors, {
          elm,
          msg
        }
      ]
    }));
  }
  checkps(){
    var jse=document.getElementById('nps');
    var jks=document.getElementById('cps');
    if(jse.value!==jks.value){
      alert('fake');
    }
    else if(jse.value=== '' && jks.value === ''  )
    {
      alert("field is empty");
    }
    else{ window.location.assign("./selection"); }
    
  
    
}

  
  clearValidationErr(elm) {
    this.setState((prevState) => {
      let newArr = [];
      
      for (let err of prevState.errors) {
        if (elm !== err.elm) {
          newArr.push(err);
        }
      }
      return {errors: newArr};
    });
  }


  onUsernameChange(e) {
    this.setState({username: e.target.value});
    
    this.clearValidationErr("username");
  }
  
 
  onPasswordChange(e) {
    this.setState({password: e.target.value});
    this.clearValidationErr("password");
    this.setState({pwdState: "weak"});
    if (e.target.value.length > 8 &&e.target.value.length<12) {
      this.setState({pwdState: "medium"});
    } else if (e.target.value.length > 12) {
      this.setState({pwdState: "strong"});
    }





  }

 






  submitRegister(e) {
    if (this.state.username === "") {
      this.showValidationErr("username", "OTP Cannot be empty!");
    }
    
    if (this.state.password === "") {
      this.showValidationErr("password", "Password Cannot be empty!");
    }
     
   


  }
  
 
  render()
  {let usernameErr = null,
    passwordErr = null;
    // emailErr = null;

  for (let err of this.state.errors) {
    
    if (err.elm === "username") {
      usernameErr = err.msg;
    }
    if (err.elm === "password") {
      passwordErr = err.msg;
    }
   
   
  }
  let pwdWeak = false,
  pwdMedium = false,
  pwdStrong = false;
//Weak password set onlt the pwdWeak to true, cause render only the first bar 
if (this.state.pwdState === "weak") {
  pwdWeak = true;
} else if (this.state.pwdState === "medium") {
  //Medium pwd then render the weak and medium bars 
  pwdWeak = true;
  pwdMedium = true;
} else if (this.state.pwdState === "strong") {
  //Strong, render all the previoud bars 
  pwdWeak = true;
  pwdMedium = true;
  pwdStrong = true;
}







    return (
      <div className="inner-container">
        <div className="header">
          Forget Password

        </div>
        <div className="box">

          <div className="input-group">
            <label htmlFor="UserName">OTP</label>
            <input
              type="text"
              name="Name"
              className="login-input"
              placeholder="OTP"
              onChange={this.onUsernameChange.bind(this)}
             
              />
                <small className="danger-error">{usernameErr? usernameErr : ""}
                      </small>
          </div>

          <div className="input-group">
            <label htmlFor="password" >New Password</label>
            <div data-tooltip="Your password should contain a capital letter...etc" >
            <input
              type="password"
              name="password"
              id="nps"
              className="login-input"
              placeholder="Password"
              onChange={this.onPasswordChange.bind(this)}
              onClick={this
            .submitRegister
            .bind(this)}
              />
              <small className="danger-error">{passwordErr? passwordErr:""}
                    </small>
                    {this.state.password && <div className="password-state">
    <div
      className={"pwd pwd-weak " + (pwdWeak? "show": "")}></div>
    <div
      className={"pwd pwd-medium " + (pwdMedium? "show": "")}></div>
    <div
      className={"pwd pwd-strong " + (pwdStrong? "show": "")}></div>
  </div>}
    
  </div>
          
          </div>
          <div className="input-group">
            <label htmlFor=" password" >Confirm Password</label>
            <div data-tooltip="Your password should contain a capital letter...etc" >
            <input
              type="password"
              
              name="password"
              className="login-input"
              placeholder=" Password"
              id="cps"
              onChange={this.onPasswordChange.bind(this)}
              onClick={this
            .submitRegister
            .bind(this)}
            required
              />
                <small className="danger-error">{passwordErr? passwordErr:""}
                    </small>
                    
    
                    </div>     
          </div>
          <button
            type="button"
            className="login-btn"
            
            
            onClick={this.checkps}
          ><Link to ="/selection">Register</Link></button>
        </div>
      </div>
    );
  }
}











export default Reset
